package cl.yerkopailemilla.falabella

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PruebaTecnicaFalabellaApp : Application()