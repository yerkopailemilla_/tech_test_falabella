package cl.yerkopailemilla.falabella.presentation.screens.movieDetail

import cl.yerkopailemilla.falabella.domain.models.MovieDetail

data class MovieDetailState(
    val movieDetail: MovieDetail? = null,
    val isLoading: Boolean = false
)