package cl.yerkopailemilla.falabella.presentation.screens.home

import cl.yerkopailemilla.falabella.domain.models.Movies

data class HomeState(
    val movies: List<Movies> = emptyList(),
    val isLoading: Boolean = false
)
