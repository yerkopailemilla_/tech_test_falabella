package cl.yerkopailemilla.falabella.presentation.navigation

object Graph {
    const val ROOT = "root_graph"
    const val MAIN = "main_graph"
}