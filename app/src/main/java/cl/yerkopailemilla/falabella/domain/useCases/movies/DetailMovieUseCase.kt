package cl.yerkopailemilla.falabella.domain.useCases.movies

import cl.yerkopailemilla.falabella.domain.models.MovieDetail
import cl.yerkopailemilla.falabella.domain.models.Response
import cl.yerkopailemilla.falabella.domain.repository.MoviesRepository
import javax.inject.Inject

class DetailMovieUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(idMovie: Int): Response<MovieDetail> {
        return repository.getMovieDetail(idMovie)
    }
}