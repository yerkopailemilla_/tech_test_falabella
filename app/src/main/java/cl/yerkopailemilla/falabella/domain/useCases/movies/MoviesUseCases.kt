package cl.yerkopailemilla.falabella.domain.useCases.movies

data class MoviesUseCases(
    val getMovies: GetMoviesUseCase,
    val detailMovie: DetailMovieUseCase
)