package cl.yerkopailemilla.falabella.domain.repository

import cl.yerkopailemilla.falabella.domain.models.MovieDetail
import cl.yerkopailemilla.falabella.domain.models.Response
import kotlinx.coroutines.flow.Flow
import cl.yerkopailemilla.falabella.domain.models.Movies

interface MoviesRepository {
    fun getMovies(): Flow<Response<List<Movies>>>
    suspend fun getMovieDetail(idMovie: Int): Response<MovieDetail>
}