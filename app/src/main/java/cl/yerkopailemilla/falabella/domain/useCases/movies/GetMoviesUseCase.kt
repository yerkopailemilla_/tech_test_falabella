package cl.yerkopailemilla.falabella.domain.useCases.movies

import cl.yerkopailemilla.falabella.domain.models.Movies
import cl.yerkopailemilla.falabella.domain.models.Response
import cl.yerkopailemilla.falabella.domain.repository.MoviesRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(private val repository: MoviesRepository) {
    operator fun invoke(): Flow<Response<List<Movies>>> {
        return repository.getMovies()
    }
}